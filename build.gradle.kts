buildscript {

    val kotlinVersion = "1.5.20"
    val hiltVersion = "2.39.1"

    extra["kotlinVersion"] = kotlinVersion
    extra["workManagerVersion"] = "2.6.0-alpha02"

    extra["hiltVersion"] = hiltVersion
    extra["androidxHiltVersion"] = "1.0.0-beta01"

    extra["roomVersion"] = "2.4.0-alpha02"
    extra["retrofitVersion"] = "2.9.0"

    repositories {
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.0.2")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hiltVersion")
        classpath("org.jlleitschuh.gradle:ktlint-gradle:10.0.0")
        classpath("de.mannodermaus.gradle.plugins:android-junit5:1.6.2.0")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.0-M1")
    }
}

allprojects {
    tasks.withType<JavaCompile> {
        options.compilerArgs.add("-Xlint:deprecation")
    }
    apply(plugin = "org.jlleitschuh.gradle.ktlint")
}
