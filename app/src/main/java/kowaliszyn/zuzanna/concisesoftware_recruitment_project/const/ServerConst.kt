package kowaliszyn.zuzanna.concisesoftware_recruitment_project.const

object ServerConst {
    const val URL = "https://api.npoint.io/"
    const val ICON_URL = "http://openweathermap.org/img/w/%s.png"
}
