package kowaliszyn.zuzanna.concisesoftware_recruitment_project.const

object ExtraConst {
    const val EXTRA_DETAILS_CITY_NAME = "detailsCityName"
    const val EXTRA_DETAILS_WEATHER = "detailsWeather"
}
