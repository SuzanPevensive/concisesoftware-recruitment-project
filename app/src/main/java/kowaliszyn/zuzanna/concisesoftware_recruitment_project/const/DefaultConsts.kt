package kowaliszyn.zuzanna.concisesoftware_recruitment_project.const

object DefaultConsts {

    const val STRING = ""
    const val BOOLEAN = false
    const val INTEGER = -1
    const val FLOAT = INTEGER.toFloat()
    const val LONG = INTEGER.toLong()
    const val DOUBLE = INTEGER.toDouble()

    inline fun <reified T> defIfNull(value: T? = null): T {
        return value ?: when (T::class.simpleName) {
            String::class.simpleName -> STRING
            Boolean::class.simpleName -> BOOLEAN
            Int::class.simpleName -> INTEGER
            Float::class.simpleName -> FLOAT
            Long::class.simpleName -> LONG
            Double::class.simpleName -> DOUBLE
            else -> INTEGER
        } as T
    }
}
