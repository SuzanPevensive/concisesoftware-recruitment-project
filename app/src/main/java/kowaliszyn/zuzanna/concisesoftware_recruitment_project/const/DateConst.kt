package kowaliszyn.zuzanna.concisesoftware_recruitment_project.const

import android.content.Context
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import java.text.SimpleDateFormat
import java.util.*

object DateConst {
    fun getDefaultDateFormatter(context: Context) =
        SimpleDateFormat(context.getString(R.string.default_date_format), Locale.ENGLISH)
}
