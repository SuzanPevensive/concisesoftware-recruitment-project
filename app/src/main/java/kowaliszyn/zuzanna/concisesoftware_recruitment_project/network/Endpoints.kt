package kowaliszyn.zuzanna.concisesoftware_recruitment_project.network

import io.reactivex.Single
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.PathsConst
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.dto.*
import retrofit2.http.*

interface Endpoints {

    @POST(PathsConst.PATH_GET_CITY_WEATHER)
    fun getCityWeather(): Single<CityWeatherResponseDto?>
}
