package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

open class UniqueLiveData<T> : ScopedLiveData<T> {

    constructor(value: T) : super(value)
    constructor() : super()

    private val initialValue = value
    private var lastValues = mutableMapOf<Observer<in T>, T?>()

    private fun uniqueObserver(observer: Observer<in T>) = Observer<T> {
        if (lastValues[observer] != it) {
            lastValues[observer] = it
            observer.onChanged(it)
        }
    }.apply {
        if (!lastValues.contains(observer)) lastValues[observer] = initialValue
    }

    override fun observeForever(observer: Observer<in T>) {
        super.observeForever(uniqueObserver(observer))
    }

    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        super.observe(owner, uniqueObserver(observer))
    }
}
