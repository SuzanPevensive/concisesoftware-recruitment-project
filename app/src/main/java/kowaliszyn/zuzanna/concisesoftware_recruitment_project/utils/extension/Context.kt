package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension

import android.content.Context
import android.widget.Toast

fun Context.showToast(resId: Int, duration: Int = Toast.LENGTH_LONG) = showToast(
    getString(resId),
    duration
)

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}
