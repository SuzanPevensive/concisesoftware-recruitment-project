package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.dataBindingAdapter

import android.view.View
import androidx.databinding.BindingAdapter

object DataBindingAdapterVisibility {

    @JvmStatic
    @BindingAdapter(value = ["isVisibility", "invisibleInsteadGone"])
    fun setVisibility(view: View, visibility: Boolean, invisibleInsteadGone: Boolean) {
        view.visibility = when {
            visibility -> View.VISIBLE
            invisibleInsteadGone -> View.INVISIBLE
            else -> View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("isVisibility")
    fun setVisibility(view: View, visibility: Boolean) {
        view.visibility = if (visibility) View.VISIBLE else View.GONE
    }
}
