package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.dataBindingAdapter

import android.view.View
import androidx.databinding.BindingAdapter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.event.impl.SingleClickEvent

object DataBindingAdapterEvent {

    interface OnSingleClickListener {
        fun onSingleClick(event: SingleClickEvent)
    }

    @JvmStatic
    @BindingAdapter("onSingleClick")
    fun setOnSingleClickListener(view: View, singleClickListener: OnSingleClickListener) {
        view.apply {
            isFocusable = false
            setOnClickListener {
                isClickable = false
                singleClickListener.onSingleClick(SingleClickEvent(this))
            }
        }
    }
}
