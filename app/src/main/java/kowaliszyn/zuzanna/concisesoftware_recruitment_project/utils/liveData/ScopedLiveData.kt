package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData

import android.os.Looper
import androidx.lifecycle.MutableLiveData

open class ScopedLiveData<T> : MutableLiveData<T> {

    constructor(value: T) : super(value)
    constructor() : super()

    override fun setValue(value: T?) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.setValue(value)
        } else {
            postValue(value)
        }
    }
}
