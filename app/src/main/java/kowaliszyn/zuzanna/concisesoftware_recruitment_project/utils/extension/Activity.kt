package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension

import android.app.Activity
import java.util.*
import kotlin.concurrent.timerTask

fun Activity.uiTimer(delay: Long, action: () -> Unit) = this.uiTimer("", delay, action)
fun Activity.uiTimer(name: String, delay: Long, action: () -> Unit) {
    val timer = if (name.isEmpty()) Timer(false) else Timer(name, false)
    timer.schedule(
        timerTask {
            runOnUiThread {
                action.invoke()
            }
        },
        delay
    )
}
