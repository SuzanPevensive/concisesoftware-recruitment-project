package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.dataBindingAdapter

import android.annotation.SuppressLint
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.decimalFormat
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SetTextI18n")
object DataBindingAdapterFormat {

    @JvmStatic
    @BindingAdapter(value = ["android:text", "unit"])
    fun setTextWithUnit(view: View, text: String, unit: String) {
        if (view is TextView) {
            if (text.isEmpty()) view.text = ""
            else view.text = "$text $unit"
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["date", "dateFormat"])
    fun setTextFromDate(view: View, date: Date, format: String) {
        if (view is TextView) {
            view.text = SimpleDateFormat(format, Locale.ENGLISH).format(date)
        }
    }

    @JvmStatic
    @BindingAdapter("decimal")
    fun setTextFromDecimal(view: View, decimal: Double?) {
        if (view is TextView) {
            view.text = decimal?.decimalFormat(2) ?: ""
        }
    }

    @JvmStatic
    @BindingAdapter(value = ["decimal", "unit"])
    fun setTextFromDecimal(view: View, decimal: Double?, unit: String) {
        setTextWithUnit(view, decimal?.decimalFormat(2) ?: "", unit)
    }
}
