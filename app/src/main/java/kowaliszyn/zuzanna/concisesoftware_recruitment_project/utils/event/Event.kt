package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.event

interface Event {
    fun done()
}
