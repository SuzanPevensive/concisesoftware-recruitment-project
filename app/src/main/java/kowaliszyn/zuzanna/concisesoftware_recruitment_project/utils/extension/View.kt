package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension

import android.view.View

fun View.disabled(isClickable: Boolean = false) {
    alpha = 0.4F
    setClickable(isClickable)
}
fun View.enabled(isClickable: Boolean = true) {
    alpha = 1.0F
    setClickable(isClickable)
}
