package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.view

import android.content.Context
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R

open class AnimationImageView : androidx.appcompat.widget.AppCompatImageView {

    protected var animationRepeat: Int = -1
    protected var animationDrawable: AnimatedVectorDrawable? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AnimationImageView,
            defStyleAttr,
            0
        ).apply {
            try {
                getResourceId(
                    R.styleable.AnimationImageView_animationRepeat,
                    -1
                ).let { resId ->
                    animationRepeat =
                        if (resId != -1) context.resources.getInteger(resId)
                        else getInt(R.styleable.AnimationImageView_animationRepeat, -1)
                }
                getResourceId(
                    R.styleable.AnimationImageView_animationSrc,
                    -1
                ).let { resId ->
                    if (resId != -1) {
                        ContextCompat.getDrawable(context, resId)?.let { drawable ->
                            if (drawable is AnimatedVectorDrawable) animationDrawable = drawable
                        }
                    }
                }
                start()
            } finally {
                recycle()
            }
            bringToFront()
        }
    }

    protected fun start() {
        animationDrawable?.let { animation ->
            var animationRepeated = 0
            AnimatedVectorDrawableCompat.registerAnimationCallback(
                animationDrawable,
                object : Animatable2Compat.AnimationCallback() {
                    override fun onAnimationEnd(drawable: Drawable?) {
                        if (animationRepeat == -1) animation.start()
                        else {
                            animationRepeated += 1
                            if (animationRepeated < animationRepeat) animation.start()
                        }
                    }
                }
            )
            animation.start()
            setImageDrawable(animation)
        }
    }
}
