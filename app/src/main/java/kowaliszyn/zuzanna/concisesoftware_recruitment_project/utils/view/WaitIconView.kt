package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.view

import android.content.Context
import android.graphics.drawable.AnimatedVectorDrawable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R

class WaitIconView : AnimationImageView {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {

        if (animationDrawable == null) {
            animationDrawable = ContextCompat.getDrawable(
                context,
                R.drawable.anim_loader
            ) as AnimatedVectorDrawable?
            start()
        }
    }
}
