package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension

fun Number.decimalFormat(digits: Int) = "%.${digits}f".format(this)
