package kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.event.impl

import android.view.View
import androidx.annotation.VisibleForTesting
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.event.Event
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.disabled
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.enabled

open class SingleClickEvent(vararg views: View) : Event {

    @VisibleForTesting
    var isDone = false
    private val views = views.toList()

    init {
        this.views.forEach { view ->
            view.disabled()
        }
    }

    override fun done() {
        if (isDone) return
        views.forEach { view ->
            view.enabled()
        }
        isDone = true
    }
}
