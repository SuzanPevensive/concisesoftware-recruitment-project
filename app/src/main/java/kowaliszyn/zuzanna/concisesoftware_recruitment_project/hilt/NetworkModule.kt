package kowaliszyn.zuzanna.concisesoftware_recruitment_project.hilt

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.ServerConst
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.network.Endpoints
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private val logging = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }
    private val httpClient = OkHttpClient.Builder().apply {
        addInterceptor(logging)
    }

    @Provides
    fun provideEndpoints(): Endpoints {
        return Retrofit.Builder().run {
            baseUrl(ServerConst.URL)
            addConverterFactory(ScalarsConverterFactory.create())
            addConverterFactory(GsonConverterFactory.create())
            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            client(httpClient.build())
            build().create(Endpoints::class.java)
        }
    }
}
