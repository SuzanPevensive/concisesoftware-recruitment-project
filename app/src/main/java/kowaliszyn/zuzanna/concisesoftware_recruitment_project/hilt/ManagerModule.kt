package kowaliszyn.zuzanna.concisesoftware_recruitment_project.hilt

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.AssetsManager
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.NetworkManager
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.impl.AssetsManagerImpl
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.impl.NetworkManagerImpl
import javax.inject.Singleton
@Module
@InstallIn(SingletonComponent::class)
abstract class ManagerModule {

    @Binds
    @Singleton
    abstract fun bindAssetsManager(assetsManagerImpl: AssetsManagerImpl): AssetsManager

    @Binds
    @Singleton
    abstract fun bindNetworkManager(networkManagerImpl: NetworkManagerImpl): NetworkManager
}
