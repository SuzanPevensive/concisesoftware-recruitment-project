package kowaliszyn.zuzanna.concisesoftware_recruitment_project.hilt

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object LibrariesModule {
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }
    @Provides
    fun providePicasso(): Picasso {
        return Picasso.get()
    }
}
