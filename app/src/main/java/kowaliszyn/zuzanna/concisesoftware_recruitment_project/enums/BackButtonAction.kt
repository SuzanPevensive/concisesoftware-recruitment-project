package kowaliszyn.zuzanna.concisesoftware_recruitment_project.enums

enum class BackButtonAction {
    DEFAULT, FINISH, EXIT
}
