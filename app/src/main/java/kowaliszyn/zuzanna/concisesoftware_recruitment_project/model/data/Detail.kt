package kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.data

import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.decimalFormat

data class Detail(val name: String, val value: Double, val unit: String) {
    fun format() = "${value.decimalFormat(2)} $unit"
}
