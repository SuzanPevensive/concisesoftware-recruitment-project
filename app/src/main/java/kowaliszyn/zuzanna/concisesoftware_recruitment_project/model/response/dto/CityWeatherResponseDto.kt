package kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.dto

import com.google.gson.annotations.SerializedName
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.dto.base.BaseResponseDto

data class CityWeatherResponseDto(
    val city: CityDto?,
    @SerializedName("list") val weather: List<WeatherDayDto>?
) : BaseResponseDto {
    data class CityDto(val name: String?)
    data class WeatherDayDto(
        @SerializedName("dt") val date: Long?,
        val pressure: Double?,
        @SerializedName("speed") val windSpeed: Double?,
        val temp: TempDto?,
        @SerializedName("weather") val description: List<DescriptionDto>?
    ) {
        data class TempDto(
            val day: Double?,
            val min: Double?,
            val max: Double?
        )

        data class DescriptionDto(
            val icon: String?,
            @SerializedName("description") val content: String?
        )
    }
}
