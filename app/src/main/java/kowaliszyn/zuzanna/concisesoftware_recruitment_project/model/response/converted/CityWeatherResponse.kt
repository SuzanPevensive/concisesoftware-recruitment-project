package kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted

import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.base.BaseResponse
import java.util.*

data class CityWeatherResponse(
    val city: City,
    val weather: List<WeatherDay>
) : BaseResponse {
    data class City(val name: String)
    data class WeatherDay(
        val date: Date,
        val pressure: Double,
        val windSpeed: Double,
        val temp: Temp,
        val description: Description
    ) {
        data class Temp(
            val day: Double,
            val min: Double,
            val max: Double
        )

        data class Description(
            val icon: String,
            val content: String
        )
    }
}
