package kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.activity

import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.CityWeatherResponse
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.showToast
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.activity.MainActivityRepository
import javax.inject.Inject

@HiltViewModel
open class MainActivityViewModel @Inject constructor(
    repository: MainActivityRepository,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<MainActivityRepository>(repository, savedStateHandle) {

    val isWaiting = ScopedLiveData(false)
    val cityName = ScopedLiveData("")
    val averageTemperature = ScopedLiveData<Double?>()

    val weatherDays = ScopedLiveData<List<CityWeatherResponse.WeatherDay>>()

    override fun onViewModelCreated() {
        loadCityWeather()
    }

    private fun loadCityWeather() {
        isWaiting.value = true
        repository.getCityWeather().subscribe(::getCityWeather) { handleResponseError() }
    }

    private fun calculateAverageTemperature(weatherDay: List<CityWeatherResponse.WeatherDay>) =
        weatherDay.sumOf { day -> day.temp.day } / weatherDay.size

    private fun getCityWeather(cityWeatherResponse: CityWeatherResponse) {
        isWaiting.value = false
        cityWeatherResponse.apply {
            cityName.value = city.name
            averageTemperature.value = calculateAverageTemperature(weather)
            weatherDays.value = weather
        }
    }

    private fun handleResponseError() {
        isWaiting.value = false
        context.showToast(R.string.exception_received_data)
        repository.getStaticCityWeather()?.let(::getCityWeather)
    }
}
