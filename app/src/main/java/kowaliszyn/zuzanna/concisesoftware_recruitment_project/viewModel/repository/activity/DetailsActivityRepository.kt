package kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.activity

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.converter.ResponseConverter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.base.BaseRepository
import javax.inject.Inject

class DetailsActivityRepository @Inject constructor(
    @ApplicationContext context: Context,
    private val responseConverter: ResponseConverter
) : BaseRepository(context) {

    val emptyWeatherDay get() = responseConverter.convertWeatherDay(null)
}
