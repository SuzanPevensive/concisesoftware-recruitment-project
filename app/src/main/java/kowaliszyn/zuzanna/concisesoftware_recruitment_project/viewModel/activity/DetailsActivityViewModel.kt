package kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.activity

import androidx.lifecycle.SavedStateHandle
import dagger.hilt.android.lifecycle.HiltViewModel
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData.ScopedLiveData
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.base.BaseViewModel
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.activity.DetailsActivityRepository
import javax.inject.Inject

@HiltViewModel
open class DetailsActivityViewModel @Inject constructor(
    repository: DetailsActivityRepository,
    savedStateHandle: SavedStateHandle
) : BaseViewModel<DetailsActivityRepository>(
    repository,
    savedStateHandle
) {
    val cityName = ScopedLiveData("")
    val weatherDay = ScopedLiveData(repository.emptyWeatherDay)
}
