package kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.base

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.base.BaseRepository

abstract class BaseViewModel<out R : BaseRepository>(
    protected val repository: R,
    protected val savedStateHandle: SavedStateHandle
) : ViewModel() {

    protected val context get() = repository.context

    open fun onViewModelCreated() {}
}
