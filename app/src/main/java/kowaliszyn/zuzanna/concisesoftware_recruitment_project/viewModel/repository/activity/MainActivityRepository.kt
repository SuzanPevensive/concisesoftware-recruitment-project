package kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.activity

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.AssetsManager
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.NetworkManager
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.base.BaseRepository
import javax.inject.Inject

open class MainActivityRepository @Inject constructor(
    @ApplicationContext context: Context,
    private val networkManager: NetworkManager,
    private val assetsManager: AssetsManager
) : BaseRepository(context) {
    fun getCityWeather() = networkManager.getCityWeather()
    fun getStaticCityWeather() = assetsManager.getCityWeather()
}
