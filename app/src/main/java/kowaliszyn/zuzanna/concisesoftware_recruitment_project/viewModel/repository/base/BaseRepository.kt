package kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.repository.base

import android.content.Context

abstract class BaseRepository constructor(
    val context: Context
)
