package kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager

import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.*

interface AssetsManager {

    fun getCityWeather(): CityWeatherResponse?
}
