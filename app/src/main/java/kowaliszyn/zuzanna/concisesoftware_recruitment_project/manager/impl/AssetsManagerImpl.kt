package kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.impl

import android.content.Context
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.converter.ResponseConverter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.AssetsManager
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.CityWeatherResponse
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.dto.CityWeatherResponseDto
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.StringBuilder
import javax.inject.Inject

class AssetsManagerImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val gson: Gson,
    private val responseConverter: ResponseConverter,
) : AssetsManager {

    override fun getCityWeather(): CityWeatherResponse? = try {
        context.assets.open("static_weather_city.json").let { inputStream ->
            BufferedReader(InputStreamReader(inputStream)).let { reader ->
                StringBuilder().let { stringBuilder ->
                    var line: String?
                    while (reader.readLine().also { line = it } != null) {
                        stringBuilder.append(line).append('\n')
                    }
                    responseConverter.convertCityWeatherResponse(
                        gson.fromJson(
                            stringBuilder.toString(),
                            CityWeatherResponseDto::class.java
                        )
                    )
                }
            }
        }
    } catch (e: Exception) {
        null
    }
}
