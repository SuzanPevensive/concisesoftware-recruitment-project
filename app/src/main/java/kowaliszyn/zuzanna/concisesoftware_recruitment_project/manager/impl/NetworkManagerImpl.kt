package kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.impl

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.converter.ResponseConverter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager.NetworkManager
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.CityWeatherResponse
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.network.Endpoints
import javax.inject.Inject

class NetworkManagerImpl @Inject constructor(
    @ApplicationContext val context: Context,
    private val endpoints: Endpoints,
    private val responseConverter: ResponseConverter,
) : NetworkManager {

    override fun getCityWeather(): Single<CityWeatherResponse> =
        endpoints.getCityWeather().subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .map { result ->
                responseConverter.convertCityWeatherResponse(result)
            }
}
