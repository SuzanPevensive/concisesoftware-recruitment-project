package kowaliszyn.zuzanna.concisesoftware_recruitment_project.manager

import io.reactivex.Single
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.*

interface NetworkManager {

    fun getCityWeather(): Single<CityWeatherResponse>
}
