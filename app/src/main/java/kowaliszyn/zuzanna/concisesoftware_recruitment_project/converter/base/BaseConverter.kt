package kowaliszyn.zuzanna.concisesoftware_recruitment_project.converter.base

open class BaseConverter {
    protected fun <T, R> convertList(list: List<T>?, converterMethod: (T) -> R): List<R> {
        return list?.map { item ->
            converterMethod(item)
        } ?: listOf()
    }
}
