package kowaliszyn.zuzanna.concisesoftware_recruitment_project.converter

import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.DefaultConsts.defIfNull
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.converter.base.BaseConverter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.*
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.dto.*
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResponseConverter @Inject constructor() : BaseConverter() {

    private fun convertKelvinDegrees(kelvinDegrees: Double?) =
        if (kelvinDegrees == null) 0.00 else kelvinDegrees - 273.15

    fun convertWeatherDay(weatherDay: CityWeatherResponseDto.WeatherDayDto?) =
        CityWeatherResponse.WeatherDay(
            Date(defIfNull(weatherDay?.date)),
            defIfNull(weatherDay?.pressure),
            defIfNull(weatherDay?.windSpeed),
            CityWeatherResponse.WeatherDay.Temp(
                convertKelvinDegrees(weatherDay?.temp?.day),
                convertKelvinDegrees(weatherDay?.temp?.min),
                convertKelvinDegrees(weatherDay?.temp?.max),
            ),
            CityWeatherResponse.WeatherDay.Description(
                defIfNull(weatherDay?.description?.firstOrNull()?.icon),
                defIfNull(weatherDay?.description?.firstOrNull()?.content)
            )
        )

    fun convertCityWeatherResponse(cityWeatherResponse: CityWeatherResponseDto?) =
        CityWeatherResponse(
            CityWeatherResponse.City(
                defIfNull(cityWeatherResponse?.city?.name)
            ),
            convertList(cityWeatherResponse?.weather, ::convertWeatherDay)

        )
}
