package kowaliszyn.zuzanna.concisesoftware_recruitment_project

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application()
