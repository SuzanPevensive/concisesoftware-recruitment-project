package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter

import androidx.lifecycle.LifecycleOwner
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.DateConst.getDefaultDateFormatter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.databinding.ItemWeatherDayBinding
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.CityWeatherResponse
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.base.ChoosableListAdapter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.decimalFormat
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData.ScopedLiveData

class WeatherDaysAdapter constructor(
    lifecycleOwner: LifecycleOwner? = null,
    private val clickRowAction: (Int) -> Unit
) : ChoosableListAdapter<CityWeatherResponse.WeatherDay, WeatherDaysAdapter.WeatherDaysViewHolderModel, ItemWeatherDayBinding>(
    listOf(),
    lifecycleOwner,
    R.layout.item_weather_day,
    0
) {

    override var chunked = false

    override fun viewHolderModelBuilder(binding: ItemWeatherDayBinding) =
        WeatherDaysViewHolderModel(binding, clickRowAction)

    inner class WeatherDaysViewHolderModel(
        binding: ItemWeatherDayBinding,
        clickRowAction: (Int) -> Unit
    ) :
        ChoosableListAdapter.ChoosableViewHolderModel<CityWeatherResponse.WeatherDay, ItemWeatherDayBinding>(
            binding,
            clickRowAction
        ) {

        val date = ScopedLiveData("")
        val tempRange = ScopedLiveData("")
        val tempDay = ScopedLiveData("")

        override fun setData(data: CityWeatherResponse.WeatherDay) {
            date.value = getDefaultDateFormatter(binding.root.context).format(data.date)
            tempRange.value = "from ${data.temp.min.decimalFormat(2)}°C to ${data.temp.max.decimalFormat(2)}°C"
            tempDay.value = data.temp.day.decimalFormat(2)
        }
    }
}
