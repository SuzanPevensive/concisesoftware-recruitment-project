package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.DefaultConsts.defIfNull
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.databinding.ActivityMainBinding
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.enums.BackButtonAction
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.WeatherDaysAdapter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.activity.MainActivityViewModel
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainActivityViewModel>() {

    @Inject
    lateinit var gson: Gson

    override val backButtonAction = BackButtonAction.EXIT

    override val viewModel by viewModels<MainActivityViewModel>()

    override val layoutId = R.layout.activity_main

    private val weatherDaysAdapter = WeatherDaysAdapter(lifecycleOwner, ::goToDetails)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.weatherDaysRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.weatherDaysRecyclerView.adapter = weatherDaysAdapter
    }

    override fun setObservers() {
        viewModel.apply {
            weatherDays.observe(lifecycleOwner) {
                weatherDaysAdapter.setData(it)
            }
        }
    }

    private fun goToDetails(weatherDayPosition: Int) {
        viewModel.apply {
            weatherDays.value?.get(weatherDayPosition)?.let { weatherDay ->
                DetailsActivity.start(
                    this@MainActivity,
                    gson,
                    defIfNull(cityName.value),
                    weatherDay
                )
            }
        }
    }
}
