package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.activity.base

import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.BR
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.enums.BackButtonAction
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.showToast
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.uiTimer
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.base.BaseViewModel

abstract class BaseActivity<B : ViewDataBinding, out VM : BaseViewModel<*>> :
    AppCompatActivity() {

    protected abstract val viewModel: VM
    protected abstract val backButtonAction: BackButtonAction
    protected abstract val layoutId: Int

    protected lateinit var binding: B
    protected val lifecycleOwner get() = this as LifecycleOwner

    private var countBackPressed = 0

    override fun onBackPressed() {
        when (backButtonAction) {
            BackButtonAction.DEFAULT -> super.onBackPressed()
            BackButtonAction.FINISH -> finish()
            BackButtonAction.EXIT -> {
                if (countBackPressed > 0) finish()
                else {
                    countBackPressed += 1
                    showToast(R.string.toast_press_back_button_one_more_time, Toast.LENGTH_SHORT)
                    uiTimer(2500) { countBackPressed = 0 }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding =
            DataBindingUtil.inflate<B>(LayoutInflater.from(this), layoutId, null, false).apply {
                setVariable(BR.viewModel, viewModel)
                lifecycleOwner = this@BaseActivity.lifecycleOwner
            }
        viewModel.onViewModelCreated()
        setContentView(binding.root)

        setObservers()
    }

    open fun setObservers() {
    }
}
