package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.base

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData.UniqueLiveData

abstract class ChunkedListAdapter<VH : RecyclerView.ViewHolder>(
    private val offsetToLoadChunk: Int,
) : RecyclerView.Adapter<VH>() {

    var maxItemCount: Int? = null
    open var chunked = true
    private var recyclerView: RecyclerView? = null

    protected var loadingItemsCount = 0
    val loadingNextChunk = UniqueLiveData<Boolean>()

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    private fun queueNotifyChanges(action: () -> Unit) {
        recyclerView?.apply {
            if (isComputingLayout || scrollState != SCROLL_STATE_IDLE) {
                post { queueNotifyChanges(action) }
            } else post { action() }
        }
    }

    // fix for system shuffle bug
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    // fix for system shuffle bug
    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun queueDataSetChanged() = queueNotifyChanges(::notifyDataSetChanged)
    fun queueItemChanged(position: Int) = queueNotifyChanges { notifyItemChanged(position) }
    fun queueItemRangeChanged(positionStart: Int, itemCount: Int) =
        queueNotifyChanges { notifyItemRangeChanged(positionStart, itemCount) }
    fun queueItemInserted(position: Int) = queueNotifyChanges { notifyItemInserted(position) }
    fun queueItemRemoved(position: Int) = queueNotifyChanges { notifyItemRemoved(position) }
    fun queueItemRangeInserted(positionStart: Int, itemCount: Int) =
        queueNotifyChanges { notifyItemRangeInserted(positionStart, itemCount) }
    fun queueItemRangeRemoved(positionStart: Int, itemCount: Int) =
        queueNotifyChanges { notifyItemRangeRemoved(positionStart, itemCount) }

    override fun onBindViewHolder(viewHolder: VH, position: Int) {
        if (chunked) {
            loadingItemsCount = position.coerceAtLeast(loadingItemsCount)
            if (itemCount - loadingItemsCount < offsetToLoadChunk &&
                maxItemCount?.let { itemCount < it } != false
            ) loadingNextChunk.value = true
        }
    }
}
