package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter

import androidx.lifecycle.LifecycleOwner
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.databinding.ItemDetailsBinding
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.data.Detail
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.base.ViewModelListAdapter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.liveData.ScopedLiveData

class DetailsAdapter constructor(
    lifecycleOwner: LifecycleOwner? = null
) : ViewModelListAdapter<Detail, DetailsAdapter.DetailsViewHolderModel, ItemDetailsBinding>(
    listOf(),
    lifecycleOwner,
    R.layout.item_details,
    0
) {

    override var chunked = false

    override fun viewHolderModelBuilder(binding: ItemDetailsBinding) =
        DetailsViewHolderModel(binding)

    inner class DetailsViewHolderModel(binding: ItemDetailsBinding) :
        ViewModelListAdapter.ViewHolderModel<Detail, ItemDetailsBinding>(
            binding
        ) {

        val detailName = ScopedLiveData("")
        val detailValue = ScopedLiveData("")

        override fun setData(data: Detail) {
            detailName.value = data.name
            detailValue.value = data.format()
        }
    }
}
