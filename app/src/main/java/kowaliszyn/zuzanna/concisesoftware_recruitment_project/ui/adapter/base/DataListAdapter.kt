package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.base

import androidx.recyclerview.widget.RecyclerView
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.ChunkListsConst

abstract class DataListAdapter
<T, VH : RecyclerView.ViewHolder>(
    list: List<T>,
    offsetToLoadChunk: Int = ChunkListsConst.LiST_OFFSET_ITEM_TO_NEXT_PAGE
) : ChunkedListAdapter<VH>(offsetToLoadChunk) {

    protected val list = list.toMutableList()

    fun getData(): List<T> {
        return list.toList()
    }

    open fun clearData(notify: Boolean = true) {
        val removedSize = list.size
        list.clear()
        loadingItemsCount = 0
        if (notify) queueDataSetChanged()
        queueItemRangeRemoved(0, removedSize)
    }
    open fun removeItem(item: T, notify: Boolean = true) {
        list.indexOf(item).let { index -> if (index != -1) this.removeItem(index, notify) }
    }
    open fun removeItem(index: Int, notify: Boolean = true) {
        list.removeAt(index)
        if (loadingItemsCount > index) loadingItemsCount -= 1
        if (notify) queueItemRemoved(index)
    }
    open fun removeItems(items: List<T>, notify: Boolean = true) {
        items.forEach { item -> removeItem(item, false) }
        if (notify) queueDataSetChanged()
    }

    fun setData(list: List<T>, index: Int? = null, notify: Boolean = true) {
        clearData(false)
        addData(list, index, notify)
    }

    open fun addData(list: List<T>, index: Int? = null, notify: Boolean = true) {
        if (list.isEmpty()) return
        index?.let { this.list.addAll(it, list) } ?: this.list.addAll(list)
        loadingNextChunk.value = false
        if (notify) queueItemRangeInserted(index ?: this.list.size, list.size)
    }

    override fun getItemCount() = list.size
}
