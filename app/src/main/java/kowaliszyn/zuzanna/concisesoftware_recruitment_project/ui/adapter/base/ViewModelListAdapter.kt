package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.base

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.ChunkListsConst

abstract class ViewModelListAdapter
<T, VM : ViewModelListAdapter.ViewHolderModel<T, B>, B : ViewDataBinding>(
    list: List<T>,
    lifecycleOwner: LifecycleOwner? = null,
    layoutId: Int,
    offsetToLoadChunk: Int = ChunkListsConst.LiST_OFFSET_ITEM_TO_NEXT_PAGE
) : BindingListAdapter<T, ViewModelListAdapter.ViewHolder<T, VM, B>, B>(
    list,
    lifecycleOwner,
    layoutId,
    offsetToLoadChunk
) {

    abstract class ViewHolderModel<T, B : ViewDataBinding>(val binding: B) {
        var position = -1
        abstract fun setData(data: T)
    }

    protected abstract fun viewHolderModelBuilder(binding: B): VM

    open class ViewHolder<T, VM : ViewHolderModel<T, B>, B : ViewDataBinding>
    (binding: B, viewHolderModelBuilder: (B) -> VM) :
        RecyclerView.ViewHolder(binding.root) {

        protected val viewModel = viewHolderModelBuilder(binding)

        init {
            binding.setVariable(BR.viewModel, viewModel)
        }

        open fun setData(position: Int, data: T) {
            viewModel.position = position
            viewModel.setData(data)
        }
    }

    override fun onCreateViewHolder(binding: B, viewType: Int) =
        ViewHolder(binding, ::viewHolderModelBuilder)

    override fun onBindViewHolder(viewHolder: ViewHolder<T, VM, B>, position: Int) {
        super.onBindViewHolder(viewHolder, position)
        viewHolder.setData(position, list[position])
    }

    override fun getItemCount() = list.size
}
