package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.ChunkListsConst

abstract class BindingListAdapter
<T, VH : RecyclerView.ViewHolder, B : ViewDataBinding>(
    list: List<T>,
    private var lifecycleOwner: LifecycleOwner? = null,
    private val layoutId: Int,
    offsetToLoadChunk: Int = ChunkListsConst.LiST_OFFSET_ITEM_TO_NEXT_PAGE
) : DataListAdapter<T, VH>(list, offsetToLoadChunk) {

    fun joinToLifeCycle(lifecycleOwner: LifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner
    }

    abstract fun onCreateViewHolder(binding: B, viewType: Int): VH

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {

        val binding = DataBindingUtil.inflate<B>(
            LayoutInflater.from(parent.context), layoutId, parent, false
        )
        val viewHolder = onCreateViewHolder(binding, viewType)
        lifecycleOwner?.let { binding.lifecycleOwner = it }

        return viewHolder
    }
}
