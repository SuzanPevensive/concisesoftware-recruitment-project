package kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.R
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.DefaultConsts.defIfNull
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.ExtraConst
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.const.ServerConst.ICON_URL
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.databinding.ActivityDetailsBinding
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.enums.BackButtonAction
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.data.Detail
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.model.response.converted.CityWeatherResponse
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.activity.base.BaseActivity
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.ui.adapter.DetailsAdapter
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.utils.extension.showToast
import kowaliszyn.zuzanna.concisesoftware_recruitment_project.viewModel.activity.DetailsActivityViewModel
import javax.inject.Inject

@AndroidEntryPoint
class DetailsActivity : BaseActivity<ActivityDetailsBinding, DetailsActivityViewModel>() {

    override val backButtonAction = BackButtonAction.FINISH

    override val viewModel by viewModels<DetailsActivityViewModel>()

    override val layoutId = R.layout.activity_details

    private val detailsAdapter = DetailsAdapter(lifecycleOwner)

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var picasso: Picasso

    companion object {

        fun start(
            context: Context,
            gson: Gson,
            cityName: String,
            weatherDay: CityWeatherResponse.WeatherDay
        ) {
            Intent(context, DetailsActivity::class.java).apply {
                putExtras(
                    saveDetails(
                        gson,
                        bundleOf(),
                        cityName,
                        weatherDay,
                    )
                )
                context.startActivity(this)
            }
        }

        private fun saveDetails(
            gson: Gson,
            outBundle: Bundle,
            cityName: String,
            weatherDay: CityWeatherResponse.WeatherDay
        ) = outBundle.apply {
            putAll(
                bundleOf(
                    ExtraConst.EXTRA_DETAILS_CITY_NAME to cityName,
                    ExtraConst.EXTRA_DETAILS_WEATHER to gson.toJson(weatherDay),
                )
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent.extras?.let(::loadDetails) ?: incorrectDetails()
        binding.weatherDayDetailsRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.weatherDayDetailsRecyclerView.adapter = detailsAdapter
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.apply {
            weatherDay.value?.let {
                saveDetails(
                    gson,
                    outState,
                    defIfNull(cityName.value),
                    it
                )
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        loadDetails(savedInstanceState)
    }

    private fun loadDetails(bundle: Bundle) {
        viewModel.cityName.value = bundle.getString(ExtraConst.EXTRA_DETAILS_CITY_NAME)
        gson.fromJson(
            bundle.getString(ExtraConst.EXTRA_DETAILS_WEATHER),
            CityWeatherResponse.WeatherDay::class.java
        ).let { weatherDay ->
            viewModel.weatherDay.value = weatherDay
            picasso
                .load(ICON_URL.format(weatherDay.description.icon))
                .placeholder(R.drawable.weather_icon_placeholder)
                .into(binding.weatherDayDescriptionIcon)
            detailsAdapter.setData(
                listOf(
                    Detail(
                        getString(R.string.details_temperature_min),
                        weatherDay.temp.min,
                        getString(R.string.unit_temp)
                    ),
                    Detail(
                        getString(R.string.details_temperature_max),
                        weatherDay.temp.max,
                        getString(R.string.unit_temp)
                    ),
                    Detail(
                        getString(R.string.details_pressure),
                        weatherDay.pressure,
                        getString(R.string.unit_pressure)
                    ),
                    Detail(
                        getString(R.string.details_wind_speed),
                        weatherDay.windSpeed,
                        getString(R.string.unit_wind_speed)
                    )
                )
            )
        }
    }

    private fun incorrectDetails() {
        showToast(R.string.exception_incorrect_details)
        finish()
    }
}
