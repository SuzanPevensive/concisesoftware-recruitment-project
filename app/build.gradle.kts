plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("de.mannodermaus.android-junit5")
}

configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
    disabledRules.set(setOf("no-wildcard-imports"))
}

android {

    compileSdk = 31

    defaultConfig {
        applicationId = "kowaliszyn.zuzanna.concisesoftware_recruitment_project"
        minSdk = 21
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility(JavaVersion.VERSION_1_8)
        targetCompatibility(JavaVersion.VERSION_1_8)
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        dataBinding = true
    }

    testOptions {
        unitTests {
            isReturnDefaultValues = true
            all {
                it.useJUnitPlatform()
            }
        }
    }

    hilt {
        enableTransformForLocalTests = true
    }
}

dependencies {

    val kotlinVersion = rootProject.extra["kotlinVersion"] as String
    val hiltVersion = rootProject.extra["hiltVersion"] as String
    val androidxHiltVersion = rootProject.extra["androidxHiltVersion"] as String
    val retrofitVersion = rootProject.extra["retrofitVersion"] as String

    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Reflection
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")

    // Core
    implementation("androidx.core:core-ktx:1.6.0")
    implementation("androidx.fragment:fragment-ktx:1.3.6")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion")

    // Gson
    implementation("com.google.code.gson:gson:2.8.6")

    // Hilt
    implementation("com.google.dagger:hilt-android:$hiltVersion")
    kapt("com.google.dagger:hilt-android-compiler:$hiltVersion")
    implementation("androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03")
    implementation("androidx.hilt:hilt-work:$androidxHiltVersion")
    kapt("androidx.hilt:hilt-compiler:$androidxHiltVersion")

    // ViewModel
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1")

    // LiveData
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.3.1")

    // RxJava
    implementation("io.reactivex.rxjava2:rxandroid:2.0.1")

    // Retrofit
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.okhttp3:logging-interceptor:3.8.0")
    implementation("com.squareup.retrofit2:converter-scalars:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-gson:$retrofitVersion")
    implementation("com.squareup.retrofit2:adapter-rxjava2:2.2.0")

    // Picasso
    implementation("com.squareup.picasso:picasso:2.8")

    // ui
    implementation("androidx.activity:activity-ktx:1.4.0-beta01")
    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.viewpager2:viewpager2:1.0.0")
    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.1")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

    // unit tests
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.0")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.0")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.7.0")
    testImplementation("org.mockito:mockito-core:3.4.0")
    testImplementation("org.mockito:mockito-inline:3.4.0")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")

    // ui tests
    androidTestImplementation("androidx.test.ext:junit:1.1.4-alpha03")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.0-alpha03")
}
