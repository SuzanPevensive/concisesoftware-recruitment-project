
# ConciseSoftware - Recruitment project

#### Recruitment project for ConciseSoftware company

App written in Kotlin using AndroidX and Ktx libraries.
App is built in `MVVM` model with `Hilt Dependency Injection`, communication between logical
layers is done with `custom Livedata extensions`. Layouts use `DataBinding` to communicate
with `ViewModels`.
Connection with server is supported by `Retrofit2` with `Gson` and `Scalars` converters.
Data transport is provided by `JavaRx`.

Project following the rules `SOLID` and `Clean Code` most of injected classes are only implementations of their interfaces.

Names of project classes is creating with `Java naming convention` and `Ktlint rules`, but resources names and ids are write with `snake_case format`.

Each default `it` argument is named according to its contents. Exceptions are "Name shadowed" situations, observers, and sometimes Collection Api functions

#### Additional comments

In the content of recruitment task was written the good practise will be using the `Navigation Component`, but in that simple application I didn't see any reason to add the navigation. App contains only list screen and screen after tap list row, I decided in this situation there is better to use two separated activities, then fragments and hidden navigation.

Horizontal screen is calculate by Activity in details screen I could load `SavedStateHandle` in viewModel, but I decided have all control about it in `Activity layer`, in particular that screen load data on create and on restore (after rotate) too.

Weather data is load from server, but if it's not available app load data from static assets json file

Icon is load from url by `Picasso` library, but if it's not available app load static placeholder

Layout was design in Photoshop file, I'm usually working with AdobeXD and I have licence only for this program, so the sizes of views and fonts were mapped by eye

##### Project structure:
- const         -     All **const** variable in project
- converter     -     Singletons to convert `dto` object to data object
- enums          -     All **enum** classes in project
- hilt          -     All `Hilt` modules and components
- manager       -     Singletons to manage all app background processes ( network connection, media provider, permissions etc. )
- model         -     Includes Requests bodies, Responses `dto`, Data objects, `Room entities`
- network       -     Directory for all endpoints and potential `Retrofit2 interceptors`
- ui            -     All files related to `Activities, Fragment and Dialogs`, includes too `adapters` and `layoutManagers` for `RecyclerView` and there are other potential classes used  to build `UI and UX`
- utils         -     All helper classes and extensions
  - dataBindingAdapter - All `DataBinding adapters` like event handlers or transport interface of complex data such as `Drawable` between `Layouts` and `ViewModels`
  - event       -     Events helper classes includes additional information and actions related with some user events, for example: `SingleClickEvent` created and send to handler by `onSingleClick DataBinding extension adapter` when user click target element. Until event object invoke **done** method user can't click again the element.
  - extension   -     Kotlin extensions of system classes
  - liveData    -     Kotlin extensions of `LiveData` interface, for example `ScopedLiveData`, which detects **current thread** and automatically run **setValue** or **postValue**, or other **SingleEventLiveData** used to single notification about some action.
  - view        -     Custom Views
- viewModel     -     All `ViewModels`, their `Repositories` and classes related to them
