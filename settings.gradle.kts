dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven("https://jcenter.bintray.com") // Warning: this repository is going to shut down soon
    }
}
rootProject.name = "ConciseSoftware - Recruitment project"
include(":app")
